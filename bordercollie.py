from datetime import datetime
from json.decoder import JSONDecodeError
from tempfile import TemporaryDirectory
from pathlib import Path
import argparse
import subprocess
import json
import os
import time
import requests
import psutil
import re
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from lib.archive import ZipInspector
#filesystem watcher code from https://thepythoncorner.com/posts/2019-01-13-how-to-create-a-watchdog-in-python-to-look-for-filesystem-changes/

webhook = None
zip_inspector = ZipInspector()

def run_semgrep_reverse_shell_check(path):
    found_shell = False
    #invoke semgrep
    run_result=subprocess.run(["semgrep",path,"--config","p/reverse-shells","--json"],capture_output=True)
    try:
        if len(run_result.stdout) > 0:
            result = json.loads(run_result.stdout)
        else:
            #no output, semgrep couldn't read
            return found_shell
    except JSONDecodeError:
        return found_shell
    if len(result['results']) > 0:
        found_shell = True
    return found_shell

def fs_remediate(path,scan_epoch_timestamp):
    print(f"remediating {path}")
    if os.name == "posix":
        os.chmod(path,0o222)
        kill_child_shells_since_timestamp(scan_epoch_timestamp)
    else:
        print("non-posix systems not currently supported")

def kill_child_shells_since_timestamp(scan_timestamp):
    for process in psutil.process_iter():
        #todo: get shell name from Semgrep rule
        if re.findall("\w{0,2}sh\b",process.name) and process.create_time >= scan_timestamp:
            process.terminate()
    return

def call_webhook(event,scan_timestamp):
    if webhook is not None:
        requests.post(webhook,json={"remediated_path":event.src_path,"scan_time":scan_timestamp})
    return

def on_change(event):
    path = event.src_path
    now_epoch = (datetime.now() - datetime(1970, 1, 1)).total_seconds()
    if zip_inspector.check_and_extract(Path(path)):
      #todo: configurable checks
      result = run_semgrep_reverse_shell_check(path)
      if result:
          fs_remediate(path,now_epoch)
          call_webhook(event,now_epoch)
      return

def run():
    #parser setup
    global webhook
    parser = argparse.ArgumentParser("Border Collie - Monitor your environment with Semgrep rules")
    parser.add_argument("--monitor_path",metavar="p",type=str,help="Root path to monitor. Defaults to root directory", default="/")
    parser.add_argument("--webhook",metavar="w",help="webhook URL to invoke when remediation occurs", default=None)
    args = parser.parse_args()
    patterns = ["*"]
    ignore_patterns = None
    ignore_directories = True
    case_sensitive = False
    eventHandler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories,case_sensitive)
    if args.webhook:
        webhook = args.webhook
    eventHandler.on_created = on_change
    eventHandler.on_modified = on_change    
    go_recursively = True
    my_observer = Observer()
    with TemporaryDirectory() as tempdir:
        blast_dir = os.path.join(tempdir,"border-collie-blast-zone")
        zip_inspector.set_blast_dir(Path(blast_dir))
        my_observer.schedule(eventHandler, blast_dir, recursive=go_recursively)
        my_observer.schedule(eventHandler, args.monitor_path, recursive=go_recursively)
        my_observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            my_observer.stop()
            my_observer.join()



if __name__ == "__main__":
    run()