# README

## Overview

Border Collie uses Semgrep and the Python `watchdog` package to detect potential reverse shells being dropped in your environment.  Big thanks to Python corner for the implementation guide of `watchdog`: https://thepythoncorner.com/posts/2019-01-13-how-to-create-a-watchdog-in-python-to-look-for-filesystem-changes/

If you aren't familiar with Semgrep, check it out [here](https://www.github.com/returntocorp/semgrep).

## Usage

Border Collie supports two arguments.

- `--monitor_path` Filesystem watcher target override (defaults to `/` - doesn't currently play nice with Docker)
- `--webhook`: Optional. The webhook to call on remediation trigger.
  - payload: `{"remediated_path":$PATH,"scan_time":$SCAN_EPOCH_TIME}`

### Example CLI call

`python3 bordercollie.py --monitor_path /foo/bar --webhook https://example.com/webhook`

## Future Work

Webhook implementations for remediation, Docker container for sidecars, maybe a Go rewrite for performance/scaling?

## Contributing

PRs are welcome!  Derivative works must follow Semgrep's license (GPL 2.1) due to embedding.