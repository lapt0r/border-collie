from zipfile import ZipFile, is_zipfile
from pathlib import Path
import os
from io import BytesIO

class ZipInspector:
  
  def __init__(self,blast_dir:Path = None):
    self.blast_directory = blast_dir
    pass

  def set_blast_dir(self,blast_dir:Path) -> None:
     self.blast_directory = blast_dir

  def check_and_extract(self,target_path:Path) -> bool:
     if not is_zipfile(target_path):
        return True
     archive = ZipFile(target_path)
     unsafe = self._check_zip_slip(archive) or self._check_zip_bomb(archive)
     if unsafe: 
        return False
     else:
        archive.extractall(self.blast_directory.name)
        return True
        

  def _check_zip_slip(self,archive:ZipFile) -> bool:
    for file in archive.infolist():
    # Check if the member's path is trying to escape the extraction directory
      if os.path.isabs(file.filename) or ".." in file.filename.split("/"):
          # The member's path is trying to escape the extraction directory
          return True
    return False

  def _check_zip_bomb(self,archive:ZipFile, max_size:int=1000000, unzip_max_size:int=20000000, max_ratio:int=100) -> bool:
    for member in archive.infolist():
        # Check if the member's compressed size is too large
        if member.compress_size > max_size:
            return True
        # Check if the member's uncompressed size is too large
        if member.file_size > unzip_max_size:
            return True
        # Check if the member's compression ratio is too high
        if member.file_size > 0 and \
                (member.compress_size / member.file_size) > max_ratio:
            return True
        # Check if the member is an archive with itself as a member
        with archive.open(member) as file:
            if is_zipfile(BytesIO(file.read())):
                return True
    # No zip bomb conditions were found
    return False