FROM python:3.9-alpine3.14
COPY . /bordercollie
RUN pip install -r /bordercollie/requirements.txt
CMD ["python3","/bordercollie/bordercollie.py"]